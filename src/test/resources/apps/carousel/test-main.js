// delaying wallaby automatic start
wallaby.delayStart();

requirejs.config({
    baseUrl: '/src',
    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap/carousel': {
            deps: ['jquery']
        }
    },
    paths: {
        // /* path to folder where individual bootstrap files have been saved. (affix.js, alert.js, etc) */
        'bootstrap': 'main/resources/etc/vendor/bootstrap.min',
        /* jQuery from CDN  */
        'jquery': 'main/resources/etc/vendor/jquery'
    }
});

require(wallaby.tests, function () {
    wallaby.start();
});