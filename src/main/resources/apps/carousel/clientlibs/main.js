// FILE: require-setup.js

require.config({
    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap/carousel': {
            deps: ['jquery']
        }
    },
    paths: {
        /* path to folder where individual bootstrap files have been saved. (affix.js, alert.js, etc) */
        'bootstrap': '../../../etc/vendor/bootstrap.min',
        'bootstrap/carousel': '../../../etc/vendor/carousel',
        /* jQuery from CDN  */
        'jquery': '../../../etc/vendor/jquery'
    }
});

// FILE: app.js
define(['jquery', 'bootstrap'], function($){
    console.log("loaded");
    // DOM ready
    $.ready(function(){

        // Twitter Bootstrap 3 carousel plugin
        $("#myCarousel").carousel();
    });
});
