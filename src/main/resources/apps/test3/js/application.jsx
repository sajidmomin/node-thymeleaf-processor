var Hello = React.createClass({
    render: function() {
        return (
            <div>
                <p>{this.props.data.title}</p>
                <p>{this.props.data.subtitle}</p>
            </div>
        );
    },
});

renderServer = function (content) {
    var data = {
        'title': content.title,
        'subtitle': content.subtitle
    };
    return ReactDOMServer.renderToString(
        <Hello data={data}/>
    );
};