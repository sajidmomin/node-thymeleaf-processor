package com.audi.sling;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

/**
 * Created by mominsa on 2/6/17.
 */
@Model(adaptables = Resource.class)
public class TestContent {

    @Inject
    private String title;

    public String getTitle() {
        return title;
    }
}
