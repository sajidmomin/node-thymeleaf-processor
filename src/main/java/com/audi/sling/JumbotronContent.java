package com.audi.sling;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

/**
 * Created by mominsa on 2/10/17.
 */
@Model(adaptables = Resource.class)
public class JumbotronContent {
    @Inject
    private String title;

    @Inject
    private String description;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
