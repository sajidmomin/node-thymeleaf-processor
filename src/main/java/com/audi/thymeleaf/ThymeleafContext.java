package com.audi.thymeleaf;

import org.thymeleaf.context.IContext;

import javax.script.Bindings;
import javax.script.ScriptContext;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Created by loom on 04.05.15.
 */
public class ThymeleafContext implements IContext {

    private final Map<String, Object> variables = new HashMap<String, Object>();
    private Locale locale;

    public ThymeleafContext(final ScriptContext scriptContext) {
        final Bindings engineBindings = scriptContext.getBindings(ScriptContext.ENGINE_SCOPE);
        variables.putAll(engineBindings);
        locale = (Locale) variables.get("locale");
    }

    @Override
    public Set<String> getVariableNames() {
        return variables.keySet();
    }

    @Override
    public Object getVariable(final String variable) {
        return variables.get(variable);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public boolean containsVariable(final String variable) {
        return variables.containsKey(variable);
    }
}
