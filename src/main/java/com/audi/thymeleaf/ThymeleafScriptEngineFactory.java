package com.audi.thymeleaf;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import java.util.Collections;
import java.util.List;

/**
 * Created by loom on 04.05.15.
 */
public class ThymeleafScriptEngineFactory implements ScriptEngineFactory {
    private final static List<String> extensions = Collections.singletonList("html");
    private final static List<String> names = Collections.singletonList("thymeleaf");
    private final static List<String> mimeTypes = Collections.singletonList("text/html");

    @Override
    public String getEngineName() {
        return "Thymeleaf";
    }

    @Override
    public String getEngineVersion() {
        return "1.0";
    }

    @Override
    public List<String> getExtensions() {
        return extensions;
    }

    @Override
    public List<String> getMimeTypes() {
        return mimeTypes;
    }

    @Override
    public List<String> getNames() {
        return names;
    }

    @Override
    public String getLanguageName() {
        return "thymeleaf";
    }

    @Override
    public String getLanguageVersion() {
        return "3.0.3.RELEASE";
    }

    @Override
    public Object getParameter(final String name) {
        return "javax.script.engine".equals(name) ?
                this.getEngineName() : ("javax.script.engine_version".equals(name) ?
                this.getEngineVersion() : ("javax.script.name".equals(name) ?
                this.getNames() : ("javax.script.language".equals(name) ?
                this.getLanguageName() : ("javax.script.language_version".equals(name) ?
                this.getLanguageVersion() : null))));
    }

    @Override
    public String getMethodCallSyntax(final String obj, final String m, final String... args) {
        final StringBuilder callSyntax = new StringBuilder();
        callSyntax.append(obj).append('.').append(m).append('(');

        for(int i = 0; args != null && i < args.length; ++i) {
            if(i > 0) {
                callSyntax.append(',');
            }

            callSyntax.append(args[i]);
        }

        callSyntax.append(')');
        return callSyntax.toString();
    }

    @Override
    public String getOutputStatement(final String value) {
        return "out.print(" + value + ")";
    }

    @Override
    public String getProgram(final String... statements) {
        return null;
    }

    @Override
    public ScriptEngine getScriptEngine() {
        return new ThymeleafScriptEngine(this);
    }
}
