package com.audi.thymeleaf.standard.expression;

import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.standard.expression.IStandardVariableExpression;
import org.thymeleaf.standard.expression.IStandardVariableExpressionEvaluator;
import org.thymeleaf.standard.expression.OGNLVariableExpressionEvaluator;
import org.thymeleaf.standard.expression.StandardExpressionExecutionContext;

/**
 * Created by mominsa on 2/6/17.
 */
public class AudiVariableExpressionEvaluator implements IStandardVariableExpressionEvaluator {


    @Override
    public Object evaluate(final IExpressionContext context,
                           final IStandardVariableExpression expression,
                           final StandardExpressionExecutionContext expContext) {
        final OGNLVariableExpressionEvaluator variableExpressionEvaluator = new OGNLVariableExpressionEvaluator(false);
        try {
            return variableExpressionEvaluator.evaluate(context, expression, expContext);
        } catch (Exception e) {
            return null;
        }
    }
}
