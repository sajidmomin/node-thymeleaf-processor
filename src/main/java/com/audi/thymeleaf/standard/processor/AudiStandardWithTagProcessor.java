package com.audi.thymeleaf.standard.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

/**
 * Created by mominsa on 2/6/17.
 */
public class AudiStandardWithTagProcessor extends AbstractAttributeTagProcessor {
    public static final int PRECEDENCE = 1600;
    private static final Logger LOGGER = LoggerFactory.getLogger(AudiStandardWithTagProcessor.class);

    public AudiStandardWithTagProcessor(final TemplateMode templateMode,
                                        final String dialectPrefix) {
        super(templateMode, dialectPrefix, null, false,
                org.thymeleaf.standard.processor.StandardWithTagProcessor.ATTR_NAME,
                true, PRECEDENCE, true);
    }

    @Override
    protected void doProcess(final ITemplateContext context,
                             final IProcessableElementTag tag,
                             final AttributeName attributeName,
                             final String attributeValue,
                             final IElementTagStructureHandler structureHandler) {
        LOGGER.info("doProcess");
    }
}
