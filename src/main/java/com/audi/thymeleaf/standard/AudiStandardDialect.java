package com.audi.thymeleaf.standard;

import com.audi.thymeleaf.standard.expression.AudiVariableExpressionEvaluator;
import com.audi.thymeleaf.standard.processor.AudiStandardWithTagProcessor;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.Map;
import java.util.Set;

/**
 * Created by mominsa on 2/7/17.
 */
public class AudiStandardDialect extends StandardDialect {

    public Set<IProcessor> getProcessors(final String dialectPrefix) {
        final Set<IProcessor> processors = createStandardProcessorsSet(dialectPrefix);
        processors.removeIf(p ->
                p.getTemplateMode() == TemplateMode.HTML &&
                        p.getClass().getCanonicalName()
                                .equals("org.thymeleaf.standard.processor.StandardWithTagProcessor"));
        final AudiStandardWithTagProcessor standardWithTagProcessor
                    = new AudiStandardWithTagProcessor(TemplateMode.HTML, StandardDialect.PREFIX);
        processors.add(standardWithTagProcessor);

        return processors;
    }

    public Map<String, Object> getExecutionAttributes() {
        setVariableExpressionEvaluator(new AudiVariableExpressionEvaluator());
        return super.getExecutionAttributes();
    }
}
