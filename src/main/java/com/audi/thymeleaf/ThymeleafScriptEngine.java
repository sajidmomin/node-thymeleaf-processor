package com.audi.thymeleaf;

import com.audi.thymeleaf.standard.AudiStandardDialect;
import org.apache.commons.io.IOUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.messageresolver.StandardMessageResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import javax.script.AbstractScriptEngine;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 *
 */
public class ThymeleafScriptEngine extends AbstractScriptEngine {

    private final ThymeleafScriptEngineFactory factory;
    private TemplateEngine templateEngine;
    /**
     *
     * @param factory
     */
    public ThymeleafScriptEngine(final ThymeleafScriptEngineFactory factory) {
        this.factory = factory;
        templateEngine = new TemplateEngine();
        templateEngine.setMessageResolver(new StandardMessageResolver());
        // Clears out the Thymeleaf Standard Dialect and uses the Audi Standard Dialog for Front-end development.
        templateEngine.clearDialects();
        templateEngine.setDialect(new AudiStandardDialect());
    }

    /**
     *
     * @param script
     * @param context
     * @return
     * @throws ScriptException
     */
    @Override
    public Object eval(final String script,
                       final ScriptContext context) throws ScriptException {
        final FileTemplateResolver resolver = new FileTemplateResolver();
        resolver.setTemplateMode(TemplateMode.HTML);
        return eval(script, context, resolver);
    }

    /**
     *
     * @param reader
     * @param context
     * @return
     * @throws ScriptException
     */
    @Override
    public Object eval(final Reader reader,
                       final ScriptContext context) throws ScriptException {
        final StringBuilder script = new StringBuilder();
        final BufferedReader bufferedReader = new BufferedReader(reader);
        try {
            while (bufferedReader.ready()) {
                script.append(bufferedReader.readLine());
                script.append("\n");
            }
            final StringTemplateResolver resolver = new StringTemplateResolver();
            resolver.setTemplateMode(TemplateMode.HTML);

            return eval(script.toString(), context, resolver);
        } catch (IOException ioe) {
            throw new ScriptException(ioe);
        } finally {
            IOUtils.closeQuietly(bufferedReader);
        }
    }

    /**
     *
     * @param script
     * @param context
     * @param resolver
     * @return
     * @throws ScriptException
     */
    public Object eval(final String script,
                       final ScriptContext context,
                       final ITemplateResolver resolver) throws ScriptException {
        templateEngine.setTemplateResolver(resolver);
        final ThymeleafContext thymeleafContext = new ThymeleafContext(context);
        final String result = templateEngine.process(script, thymeleafContext);
        try {
            context.getWriter().append(result);
        } catch (IOException e) {
            throw new ScriptException(e);
        }
        return result;
    }

    /**
     *
     * @return
     */
    @Override
    public Bindings createBindings() {
        return new SimpleBindings();
    }

    /**
     *
     * @return
     */
    @Override
    public ScriptEngineFactory getFactory() {
        return factory;
    }
}
