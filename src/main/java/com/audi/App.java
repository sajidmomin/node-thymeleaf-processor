package com.audi ;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleScriptContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static Object process(final String[] args) {
        LOGGER.info("Compile Thymeleaf Template");

        final String directoryPath = args[0];
        final String fileStem = args[1];
        final String fileExtName = args[2];
        LOGGER.info("directoryPath = " + directoryPath);
        LOGGER.info("fileStem = " + fileStem);
        LOGGER.info("fileExtName = " + fileExtName);

        final String viewFilePath = directoryPath + "/" + fileStem + fileExtName;
        final String jsonFilePath = directoryPath + "/" + fileStem + ".json";
        LOGGER.info("viewFilePath = " + viewFilePath);
        LOGGER.info("jsonFilePath = " + jsonFilePath);

        try {
            final InputStream in = new FileInputStream(new File(jsonFilePath));
            final ObjectMapper mapper = new ObjectMapper();

            final Map<String, Object> content = mapper.readValue(in, new TypeReference<Map<String, Object>>() {});
            final ScriptContext scriptContext = new SimpleScriptContext();
            scriptContext.setAttribute("locale", Locale.ENGLISH, ScriptContext.ENGINE_SCOPE);
            scriptContext.setAttribute("content", content, ScriptContext.ENGINE_SCOPE);

            final ScriptEngineManager manager = new ScriptEngineManager();
            final ScriptEngine engine = manager.getEngineByExtension(fileExtName.replace(".", ""));

            final Object result = (fileExtName.contains(".html")) ?
                    engine.eval(viewFilePath, scriptContext) :
                    engine.eval(new FileReader(viewFilePath), scriptContext);

            LOGGER.info(result.toString());

            return result;
        } catch (IOException | ScriptException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void main(final String[] args) {

        final ScriptEngineManager manager = new ScriptEngineManager();
        final List<ScriptEngineFactory> factories = manager.getEngineFactories();
        for (final ScriptEngineFactory f : factories) {
            System.out.println("engine name:" + f.getEngineName());
            System.out.println("engine version:" + f.getEngineVersion());
            System.out.println("language name:" + f.getLanguageName());
            System.out.println("language version:" + f.getLanguageVersion());
            System.out.println("names:" + f.getNames());
            System.out.println("mime:" + f.getMimeTypes());
            System.out.println("extension:" + f.getExtensions());
            System.out.println("-----------------------------------------------");
        }

//        System.out.println(process(args));
    }
}
