const gulp = require('gulp'),
    connect = require('gulp-connect'),
    watch = require('gulp-watch'),
    browserify = require('browserify'),
    reactify = require('reactify'),
    del = require('del'),
    java = require('java'),
    vfs = require('vinyl-fs'),
    source = require('vinyl-source-stream'),
    thymeleafProcessor = require('./gulp-thymeleaf-processor.js');
java.classpath.push('./target/node-thymeleaf-processor-1.0-SNAPSHOT-jar-with-dependencies.jar');

gulp.task('connect', function() {
    connect.server({
        root: 'target/app',
        livereload: true
    });
});

// gulp.task('stream', function () {
//     // Endless stream mode
//     return watch(['src/main/resources/apps/**/*.html','src/main/resources/apps/**/*.json'], { ignoreInitial: false })
//         .pipe(thymeleafProcessor())
//         .pipe(gulp.dest('target/app'))
//         .pipe(connect.reload());
// });

gulp.task('watch', function () {
    // Currently the watch will execute the build task which will recompile all *.html.  This watch can be optimized to only process the changed files.
    gulp.watch([
        'src/main/resources/apps/**/*.html',
        'src/main/resources/apps/**/*.json',
        'src/main/resources/apps/**/*.properties',
        'src/main/resources/apps/**/*.js',
        'src/main/resources/apps/**/*.css'
    ], ['build']);
    gulp.watch(['src/main/resources/apps/**/*.jsx'], ['build']);
    gulp.watch([
        'src/main/resources/apps/**/*.js',
        'src/main/resources/apps/**/*.css'
    ], ['copy']);

});

gulp.task('build', ['browserify'], function () {
    vfs.src([
        'src/main/resources/**/*.html',
        'src/main/resources/**/*.js',
        '!src/main/resources/apps/**/js/*.js',
        '!src/main/resources/apps/**/clientlibs/**/*.js'
    ])
        .pipe(thymeleafProcessor())
        .pipe(vfs.dest('target/app'))
        .pipe(connect.reload());
});

gulp.task('copy', function () {
    vfs.src([
        'src/main/resources/**/*.js',
        'src/main/resources/**/*.css',
        'src/main/resources/**/images/*',
        '!src/main/resources/apps/*/*.js'
    ])
        .pipe(vfs.dest('target/app'))
});

gulp.task('browserify', function () {
    // // sync
    // del.sync('src/main/resources/apps/test3/js/application.jsx');
    var b = browserify();
    b.transform(reactify); // use the reactify transform
    b.add('src/main/resources/apps/test3/js/application.jsx');
    return b.bundle()
        .pipe(source('application.js'))
        .pipe(gulp.dest('src/main/resources/apps/test3/js'))
});

gulp.task('default', ['connect', 'build', 'copy']);
