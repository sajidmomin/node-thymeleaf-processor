module.exports = function (wallaby) {
    return {
        files: [
            {pattern: 'src/main/resources/etc/vendor/require.js', instrument: false},
            {pattern: 'src/main/resources/etc/vendor/*.js', instrument: false, load: false},
            {pattern: 'src/app.js', load: false},
            {pattern: 'src/test/**/test-main.js', instrument: false}
        ],

        tests: [
            {pattern: 'src/test/**/carouselSpec.js', load: false}
        ]
    };
};