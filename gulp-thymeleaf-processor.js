const gulp = require('gulp'),
    java = require('java'),
    through = require('through2'),
    gutil = require('gulp-util');
java.classpath.push("./target/node-thymeleaf-processor-1.0-SNAPSHOT-jar-with-dependencies.jar");

// Consts
const PLUGIN_NAME = 'gulp-thymeleaf-processor';

// Plugin level function(dealing with files)
function gulpThymeleafProcessor() {
    // Creating a stream through which each file will pass
    return through.obj(function(file, enc, cb) {
        console.log(file.dirname, file.name, file.path);
        if (file.isNull()) {
            // return empty file
            return cb(null, file);
        }

        var arguments = java.newArray("java.lang.String", [file.dirname, file.stem, file.extname]);
        file.contents = new Buffer(java.callStaticMethodSync('com.audi.App', 'process', arguments));
        file.extname = ".html";

        cb(null, file);

    });

}

// Exporting the plugin main function
module.exports = gulpThymeleafProcessor;